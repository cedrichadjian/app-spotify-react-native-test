import React, { Component } from "react";

import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
const Stack = createStackNavigator();
const NavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: "black",
  },
};

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators as actions } from "./components/Login/actions";

import Login from "./components/Login";
import Search from "./components/Search";

class Navigations extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props);
    return (
      <NavigationContainer theme={NavigationTheme}>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          {!this.props.access_token && <Stack.Screen name="Login" component={Login} />}
          {this.props.access_token && <Stack.Screen name="Search" component={Search} />}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

function mapStateToProps(state) {
  const { access_token, refresh_token, expiration_time } = state;
  return {
    access_token,
    refresh_token,
    expiration_time,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addTokens: bindActionCreators(actions.addTokens, dispatch),
    refreshTokens: bindActionCreators(actions.refreshTokens, dispatch),
    removeTokens: bindActionCreators(actions.removetokens, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigations);
