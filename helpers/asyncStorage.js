import errorHandler from "./errorHandler";
import { AsyncStorage } from "react-native";

const storeData = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, value);
  } catch (err) {
    errorHandler(err);
  }
};

const getData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value !== null ? value : null;
  } catch (error) {
    // Error retrieving data
  }
};

export default (storeData, getData);
