import { Alert } from "react-native";

const errorHandler = (err) => {
  console.error(err);
  Alert.alert("Uh oh, something went wrong.");
};

export default errorHandler;
