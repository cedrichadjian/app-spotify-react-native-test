const express = require("express"),
  bodyParser = require("body-parser"),
  app = express();
require("dotenv").config();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/api/spotify-credentials", (req, res) => {
  const clientId = process.env.appKey;
  const clientSecret = process.env.appSecret;
  const redirectUri = process.env.callbackURL;
  const spotifyCredentials = { clientId, clientSecret, redirectUri };
  res.json(spotifyCredentials);
});

app.listen(8888, function () {
  console.log("Listening on port 8888!");
});
