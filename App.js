import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./store/store";

import Navigations from "./Navigations";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigations />
      </Provider>
    );
  }
}
