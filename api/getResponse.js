import * as AuthSession from "expo-auth-session";
import { encode as btoa } from "base-64";
import axios from "axios";
import errorHandler from "../helpers/errorHandler";

const getSpotifyCredentials = async () => {
  try {
    const res = await axios.get("http://192.168.0.104:8888/api/spotify-credentials");
    const spotifyCredentials = res.data;
    return spotifyCredentials;
  } catch (err) {
    errorHandler(err);
  }
};

const getAuthorizationCode = async () => {
  try {
    const credentials = await getSpotifyCredentials();
    const redirectUrl = AuthSession.getRedirectUrl();
    const result = await AuthSession.startAsync({
      authUrl:
        "https://accounts.spotify.com/authorize" +
        "?response_type=code" +
        "&client_id=" +
        credentials.clientId +
        "&redirect_uri=" +
        encodeURIComponent(redirectUrl),
    });
    if (result.type === "success") return result.params.code;
  } catch (err) {
    errorHandler(err);
  }
};

const getResponse = async (type) => {
  try {
    const authorizationCode = await getAuthorizationCode();
    const credentials = await getSpotifyCredentials();
    const credsB64 = btoa(`${credentials.clientId}:${credentials.clientSecret}`);
    // const refreshToken = this.state.refresh_token;
    let body =
      type === "refresh"
        ? `grant_type=refresh_token&refresh_token=${refreshToken}`
        : `grant_type=authorization_code&code=${authorizationCode}&redirect_uri=${credentials.redirectUri}`;
    const response = await fetch("https://accounts.spotify.com/api/token", {
      method: "POST",
      headers: {
        Authorization: `Basic ${credsB64}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body,
    });
    return response.json();
  } catch (err) {
    errorHandler(err);
  }
};

export default getResponse;
