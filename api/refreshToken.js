import getResponse from "./getResponse";
import errorHandler from "../helpers/errorHandler";

const refreshtokens = async () => {
  try {
    const responseJson = await getResponse("refresh");
    if (responseJson.error) {
      await getTokens();
    } else {
      const { access_token: newAccessToken, refresh_token: newRefreshToken, expires_in: expiresIn } = responseJson;
      const expirationTime = new Date().getTime() + expiresIn * 1000;
      if (newRefreshToken) {
        return {
          access_token: newAccessToken,
          refresh_token: newRefreshToken,
          expiration_time: expirationTime,
        };
      }
    }
  } catch (err) {
    errorHandler(err);
  }
};

export default refreshtokens;
