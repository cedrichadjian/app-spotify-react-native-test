import getResponse from "./getResponse";
import errorHandler from "../helpers/errorHandler";
import storeData from "../helpers/asyncStorage";

const getTokens = async () => {
  try {
    const responseJson = await getResponse();
    const { access_token, refresh_token, expires_in } = responseJson;
    const expiration_time = new Date().getTime() + expires_in * 1000;
    // await storeData("access_token", access_token);
    // await storeData("refresh_tokens", refresh_token);
    // await storeData("expiration_time", expiration_time);
    return {
      access_token,
      refresh_token,
      expiration_time,
    };
  } catch (err) {
    errorHandler(err);
  }
};

export default getTokens;
