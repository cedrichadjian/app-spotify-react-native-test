import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Input } from "react-native-elements";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getSpotifyURL = () => {
    return {
      url: `/search?access_token=${this.state.access_token}&type=artist&q=${this.state.search}`,
      method: "get",
      baseURL: "https://api.spotify.com/v1/",
    };
  };

  searchTimeout = null;

  updateSearch = (search) => {
    console.log(search);
    this.setState({
      search,
    });
    this.searchTimeout = setTimeout(() => {
      Axios.request(this.getSpotifyURL())
        .then((response) => {
          let artists_list = response.data.artists.items;
          console.log(artists_list);
        })
        .catch((err) => {
          errorHandler(err);
        });
    }, 500);
  };

  render() {
    return (
      <View style={styles.searchWrapper}>
        <Input
          containerStyle={styles.search}
          placeholder="Search for artists"
          onChangeText={this.updateSearch}
          leftIcon={{ type: "font-awesome", name: "search" }}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          placeholderTextColor="black"
        ></Input>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchWrapper: {
    marginTop: 40,
    alignItems: "center",
  },
  search: {
    backgroundColor: "white",
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 10,
    borderColor: "white",
    width: 300,
    paddingBottom: 0,
  },
});

export default Search;
