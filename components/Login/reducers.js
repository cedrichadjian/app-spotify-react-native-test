import { ADD_TOKENS, REFRESH_TOKENS, REMOVE_TOKENS } from "./types";
import getTokens from "../../api/getTokens";
import refreshtokens from "../../api/refreshToken";

const initialState = {
  access_token: null,
  refresh_token: null,
  expiration_time: null,
};

async function applyAddTokens(state) {
  const token = await getTokens();
  console.log("Token inside reducer is:", token);
  return {
    ...state,
    access_token: token.access_token,
    refresh_token: token.refresh_token,
    expiration_time: token.expiration_time,
  };
}

async function applyRefreshTokens(state) {
  const tokenExpirationTime = state.expiration_time;
  if (tokenExpirationTime && new Date().getTime() > tokenExpirationTime) {
    const refreshTokens = await refreshtokens();
    return {
      ...state,
      access_token: refreshTokens.access_token,
      refresh_token: refreshTokens.refresh_token,
      expiration_time: refreshTokens.expiration_time,
    };
  }
}

function applyRemoveTokens(state) {
  return {
    ...state,
    access_token: null,
    refresh_token: null,
    expiration_time: null,
  };
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TOKENS:
      return applyAddTokens(state);
    case REFRESH_TOKENS:
      return applyRefreshTokens(state);
    case REMOVE_TOKENS:
      return applyRemoveTokens(state);
    default:
      return state;
  }
}

export default reducer;
