import { ADD_TOKENS, REFRESH_TOKENS, REMOVE_TOKENS } from "./types";

function addTokens() {
  return {
    type: ADD_TOKENS,
  };
}

function refreshTokens() {
  return {
    type: REFRESH_TOKENS,
  };
}

function removetokens() {
  return {
    type: REMOVE_TOKENS,
  };
}

const actionCreators = {
  addTokens,
  refreshTokens,
  removetokens,
};

export { actionCreators };
