import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators as actions } from "./actions";

class Login extends Component {
  render() {
    const { addTokens } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.login}>
          <TouchableOpacity onPress={addTokens}>
            <Text style={styles.login_button}>Login</Text>
          </TouchableOpacity>
          <FontAwesome style={styles.login_fa} name={"spotify"} size={40} color={"#1DB954"} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
  },
  login: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 90,
    borderColor: "#fff",
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 5,
    paddingTop: 15,
    paddingBottom: 15,
  },
  login_button: {
    alignItems: "center",
    justifyContent: "center",
    color: "#fff",
    fontSize: 20,
    paddingRight: 100,
    paddingLeft: 100,
  },
  login_fa: {
    position: "absolute",
    left: 200,
  },
});

function mapStateToProps(state) {
  const { access_token, refresh_token, expiration_time } = state;
  return {
    access_token,
    refresh_token,
    expiration_time,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addTokens: bindActionCreators(actions.addTokens, dispatch),
    refreshTokens: bindActionCreators(actions.refreshTokens, dispatch),
    removeTokens: bindActionCreators(actions.removetokens, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
